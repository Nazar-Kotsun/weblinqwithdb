using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPILinq.BLL.Services.Interfaces;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class TeamController : Controller
    {
        private readonly ITeamService _teamService;

        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        [Route("teams")]
        public async Task<IActionResult> GetAll()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _teamService.GetAllTeams();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpGet]
        [Route("teams/withUsers")]
        public async Task<IActionResult> GetTeamsWithUsers()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _teamService.GetTeamsWithUsers();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("teams")]
        public async Task<IActionResult> AddTeam([FromBody] TeamModel teamModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            string strMistakes;
            
            try
            {
                await _teamService.AddTeam(teamModel);
                return Created("", "Team was created!");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpDelete]
        [Route("teams/{teamId}")]
        public async Task<IActionResult> DeleteProject(int teamId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            string strMistakes;
            try
            {
                await _teamService.DeleteTeam(teamId);
                return Ok("Team was deleted");
            }
            catch (InvalidOperationException ex)
            {
                strMistakes = ex.Message;
            }
            catch (NullReferenceException ex)
            {
                strMistakes = ex.Message;
            }
            catch (Exception ex)
            {
                strMistakes = ex.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpPut]
        [Route("teams")]
        public async Task<IActionResult> UpdateProject(TeamModel teamModel)
        {
            string strMistakes;
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            try
            {
                await _teamService.UpdateTeam(teamModel);
                return Ok("Team was updated");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
    }
}