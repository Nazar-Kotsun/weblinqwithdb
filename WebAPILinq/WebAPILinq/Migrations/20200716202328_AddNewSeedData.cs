﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPILinq.Migrations
{
    public partial class AddNewSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                type: "nvarchar(650)",
                maxLength: 650,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                type: "nvarchar(550)",
                maxLength: 550,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "TaskStates",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tasks",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Tasks",
                type: "nvarchar(4000)",
                maxLength: 4000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(200)",
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(150)",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Projects",
                type: "nvarchar(4000)",
                maxLength: 4000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(500)",
                oldMaxLength: 500,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Positions",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100);

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "QC/QA Engineer" },
                    { 2, "Project Manager" },
                    { 3, "Developer" },
                    { 4, "Administrator" },
                    { 5, "Architect" }
                });

            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Started" },
                    { 3, "Finished" },
                    { 4, "Canceled" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 6, 30, 23, 20, 46, 396, DateTimeKind.Local).AddTicks(8861), "debitis" },
                    { 2, new DateTime(2020, 6, 30, 21, 46, 57, 1, DateTimeKind.Local).AddTicks(953), "cupiditate" },
                    { 3, new DateTime(2020, 7, 1, 6, 2, 23, 153, DateTimeKind.Local).AddTicks(8591), "asperiores" },
                    { 4, new DateTime(2020, 7, 1, 7, 38, 51, 357, DateTimeKind.Local).AddTicks(9944), "ea" },
                    { 5, new DateTime(2020, 6, 30, 18, 19, 41, 618, DateTimeKind.Local).AddTicks(9004), "amet" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "Price", "TeamId", "UserId" },
                values: new object[,]
                {
                    { 8, 16, new DateTime(2020, 7, 1, 13, 44, 14, 785, DateTimeKind.Local).AddTicks(4397), new DateTime(2020, 8, 26, 6, 56, 19, 702, DateTimeKind.Local).AddTicks(9479), "Odio numquam quis quia ut sapiente facilis molestiae esse.\nPerspiciatis enim totam repudiandae non sint similique.", "Ad vero qui culpa odit.", 100000m, 2, null },
                    { 7, 14, new DateTime(2020, 7, 1, 12, 22, 12, 920, DateTimeKind.Local).AddTicks(590), new DateTime(2021, 3, 30, 19, 55, 8, 280, DateTimeKind.Local).AddTicks(325), "Modi nisi quasi vero odio amet excepturi.\nOfficiis et a molestiae rerum.\nSuscipit ea aut autem ipsa itaque nihil.\nEum et nihil eveniet accusantium ea quod temporibus.", "Quaerat omnis enim sint ex.", 6000m, 4, null },
                    { 9, 7, new DateTime(2020, 7, 1, 15, 2, 1, 718, DateTimeKind.Local).AddTicks(6234), new DateTime(2021, 5, 12, 9, 50, 23, 127, DateTimeKind.Local).AddTicks(5980), "Deleniti voluptates tempora enim voluptas.\nVoluptatem atque praesentium vel.\nItaque eum deleniti voluptas veniam.\nSapiente mollitia dolore placeat.\nAut molestiae error eaque cum.", "In vitae neque soluta et.", 1540m, 3, null },
                    { 4, 10, new DateTime(2020, 6, 30, 23, 20, 6, 43, DateTimeKind.Local).AddTicks(3222), new DateTime(2020, 9, 5, 15, 42, 26, 276, DateTimeKind.Local).AddTicks(3701), "Voluptatem eaque accusamus maiores quo beatae quos doloremque.\nEos pariatur ea saepe atque.\nDelectus quidem voluptatem harum architecto repellat.\nCupiditate culpa consectetur illo occaecati et.\nCumque inventore voluptas tenetur.\nFacilis quaerat sed praesentium.", "Quia repellendus odit et eligendi.", 24000m, 3, null },
                    { 2, 6, new DateTime(2020, 7, 1, 2, 19, 9, 512, DateTimeKind.Local).AddTicks(2629), new DateTime(2021, 4, 30, 4, 56, 33, 393, DateTimeKind.Local).AddTicks(9752), "Aut quia id adipisci alias non mollitia.\nAlias et at quia soluta quisquam aspernatur nemo molestias.\nVel id suscipit vero ipsa repudiandae nesciunt.\nProvident veritatis maiores aut.\nIste et incidunt.", "Totam autem hic atque suscipit.", 1000m, 4, null },
                    { 5, 12, new DateTime(2020, 7, 1, 7, 41, 59, 505, DateTimeKind.Local).AddTicks(2577), new DateTime(2021, 5, 4, 5, 1, 44, 683, DateTimeKind.Local).AddTicks(3676), "Qui rem mollitia inventore nulla nam nam excepturi.\nQuibusdam distinctio iste quo dolor.\nBeatae consequatur qui est quo amet et quia.", "Aspernatur vero quas et ipsum.", 56000m, 2, null },
                    { 3, 8, new DateTime(2020, 7, 1, 15, 59, 4, 683, DateTimeKind.Local).AddTicks(5960), new DateTime(2020, 11, 7, 14, 49, 38, 502, DateTimeKind.Local).AddTicks(8811), "Quis dicta repudiandae consequatur et odio repudiandae occaecati.\nDolore fugit veniam dolorem aperiam consequatur cum sed officiis ut.\nExercitationem ea ducimus saepe id asperiores dignissimos molestiae repellat.", "Eos illum eum minima quibusdam", 3120m, 4, null },
                    { 10, 7, new DateTime(2020, 7, 1, 7, 1, 16, 628, DateTimeKind.Local).AddTicks(4161), new DateTime(2021, 1, 17, 19, 40, 52, 374, DateTimeKind.Local).AddTicks(3928), "Rem ipsam et ipsa inventore.\nQuibusdam id omnis fuga.\nEt a porro ut deleniti.", "Atque ut nisi dignissimos quas.", 90000m, 5, null },
                    { 6, 14, new DateTime(2020, 7, 1, 3, 31, 42, 923, DateTimeKind.Local).AddTicks(6504), new DateTime(2020, 7, 30, 3, 13, 23, 134, DateTimeKind.Local).AddTicks(3010), "Molestiae incidunt praesentium dolor odit culpa voluptatibus maxime et nam.\nAut nam et laudantium omnis et sed.\nOdio perspiciatis iure exercitationem possimus dicta minima.", "Reprehenderit nam architecto sed aut.", 4000m, 1, null },
                    { 1, 8, new DateTime(2020, 7, 1, 6, 15, 53, 288, DateTimeKind.Local).AddTicks(5115), new DateTime(2021, 2, 2, 2, 57, 55, 67, DateTimeKind.Local).AddTicks(7911), "Ea ab omnis saepe rem vel et.\nIllo quaerat eos accusantium reiciendis dolores quibusdam ratione.", "Expedita amet quas id a.", 20000m, 5, null }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "PositionId", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(2005, 10, 29, 0, 26, 53, 19, DateTimeKind.Local).AddTicks(3606), "Jordane.Walker@gmail.com", "Jordane", "Walker", 1, new DateTime(2020, 6, 24, 12, 11, 10, 569, DateTimeKind.Local).AddTicks(1173), 5 },
                    { 15, new DateTime(2016, 9, 5, 18, 10, 36, 509, DateTimeKind.Local).AddTicks(538), "Tamara58@gmail.com", "Tamara", "Brakus", 4, new DateTime(2020, 6, 8, 16, 4, 31, 614, DateTimeKind.Local).AddTicks(4169), 4 },
                    { 14, new DateTime(2019, 3, 31, 10, 6, 27, 880, DateTimeKind.Local).AddTicks(720), "Kirsten_Braun49@gmail.com", "Kirsten", "Braun", 2, new DateTime(2020, 6, 3, 17, 20, 9, 673, DateTimeKind.Local).AddTicks(5222), 4 },
                    { 4, new DateTime(2008, 7, 14, 23, 47, 23, 708, DateTimeKind.Local).AddTicks(7566), "Verla.Bechtelar62@gmail.com", "Verla", "Bechtelar", 5, new DateTime(2020, 7, 1, 8, 22, 15, 869, DateTimeKind.Local).AddTicks(5560), 5 },
                    { 13, new DateTime(2020, 4, 1, 8, 47, 34, 579, DateTimeKind.Local).AddTicks(4826), "Issac.Rath@gmail.com", "Issac", "Rath", 3, new DateTime(2020, 6, 18, 10, 48, 28, 954, DateTimeKind.Local).AddTicks(2609), 4 },
                    { 9, new DateTime(2019, 6, 10, 5, 27, 47, 484, DateTimeKind.Local).AddTicks(4129), "Luisa92@yahoo.com", "Luisa", "Lind", 3, new DateTime(2020, 6, 22, 22, 42, 43, 764, DateTimeKind.Local).AddTicks(2460), 4 },
                    { 8, new DateTime(2003, 4, 10, 23, 50, 3, 549, DateTimeKind.Local).AddTicks(3682), "Norma_Gislason@yahoo.com", "Norma", "Gislason", 3, new DateTime(2020, 5, 28, 20, 2, 2, 295, DateTimeKind.Local).AddTicks(5080), 5 },
                    { 11, new DateTime(2012, 9, 18, 15, 52, 12, 442, DateTimeKind.Local).AddTicks(4499), "Jayde22@hotmail.com", "Jayde", "Pouros", 1, new DateTime(2020, 6, 29, 16, 40, 13, 993, DateTimeKind.Local).AddTicks(9520), null },
                    { 5, new DateTime(2007, 9, 20, 13, 5, 41, 152, DateTimeKind.Local).AddTicks(7935), "Retha67@yahoo.com", "Retha", "Will", 1, new DateTime(2020, 6, 23, 21, 9, 18, 503, DateTimeKind.Local).AddTicks(8054), 3 },
                    { 12, new DateTime(2003, 1, 30, 19, 45, 4, 747, DateTimeKind.Local).AddTicks(7993), "Sherman_Kuhlman44@hotmail.com", "Sherman", "Kuhlman", 2, new DateTime(2020, 6, 12, 1, 12, 48, 711, DateTimeKind.Local).AddTicks(8931), 5 },
                    { 3, new DateTime(2008, 9, 29, 18, 28, 43, 736, DateTimeKind.Local).AddTicks(9629), "Mabelle.Miller12@hotmail.com", "Mabelle", "Miller", 2, new DateTime(2020, 6, 22, 15, 35, 42, 583, DateTimeKind.Local).AddTicks(4106), 3 },
                    { 17, new DateTime(2008, 4, 10, 16, 16, 49, 992, DateTimeKind.Local).AddTicks(6847), "Delmer9@gmail.com", "Delmer", "Wunsch", 3, new DateTime(2020, 5, 28, 0, 57, 23, 539, DateTimeKind.Local).AddTicks(7637), 2 },
                    { 16, new DateTime(2010, 11, 19, 23, 50, 45, 858, DateTimeKind.Local).AddTicks(7891), "Hilton.Kemmer84@gmail.com", "Hilton", "Kemmer", 4, new DateTime(2020, 7, 1, 10, 0, 16, 686, DateTimeKind.Local).AddTicks(452), 2 },
                    { 19, new DateTime(2004, 11, 26, 8, 18, 16, 796, DateTimeKind.Local).AddTicks(8598), "edrick33@hotmail.com", "Cedrick", "Lemke", 1, new DateTime(2020, 5, 22, 7, 53, 58, 617, DateTimeKind.Local).AddTicks(4950), 1 },
                    { 6, new DateTime(2007, 2, 27, 1, 18, 31, 140, DateTimeKind.Local).AddTicks(5084), "Roberto.Ruecker35@yahoo.com", "Roberto", "Ruecker", 3, new DateTime(2020, 6, 2, 2, 17, 26, 430, DateTimeKind.Local).AddTicks(9832), 1 },
                    { 2, new DateTime(2012, 1, 23, 12, 37, 51, 941, DateTimeKind.Local).AddTicks(2291), "Kolby_Jones@yahoo.com", "Kolby", "Jones", 4, new DateTime(20, 6, 27, 22, 22, 55, 794, DateTimeKind.Local).AddTicks(233), 1 },
                    { 10, new DateTime(2008, 1, 1, 11, 16, 36, 454, DateTimeKind.Local).AddTicks(8263), "Edward_Kuvalis@yahoo.com", "Edward", "Kuvalis", 5, new DateTime(2020, 6, 3, 2, 16, 31, 267, DateTimeKind.Local).AddTicks(7270), null },
                    { 20, new DateTime(2016, 10, 17, 14, 55, 25, 834, DateTimeKind.Local).AddTicks(3670), "Zane.Will@yahoo.com", "Zane", "Will", 3, new DateTime(2020, 6, 5, 14, 13, 47, 441, DateTimeKind.Local).AddTicks(5614), null },
                    { 7, new DateTime(2003, 7, 8, 15, 0, 24, 375, DateTimeKind.Local).AddTicks(7499), "Tracy.Schinner65@gmail.com", "Tracy", "Schinner", 3, new DateTime(2020, 6, 23, 17, 37, 38, 49, DateTimeKind.Local).AddTicks(8331), 3 },
                    { 18, new DateTime(2002, 7, 26, 23, 27, 55, 982, DateTimeKind.Local).AddTicks(7619), "Lupe91@gmail.com", "Lupe", "Harber", 3, new DateTime(2020, 5, 22, 20, 21, 56, 99, DateTimeKind.Local).AddTicks(8576), 5 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State", "TaskStateId", "UserId" },
                values: new object[,]
                {
                    { 6, new DateTime(2020, 7, 1, 9, 47, 59, 892, DateTimeKind.Local).AddTicks(9665), "Sit tenetur nihil laborum qui quia assumenda ratione.\nTempora esse deleniti quia debitis incidunt odio consequatur unde.\nEt quos quam consectetur excepturi sint qui enim autem eaque.\nAdipisci praesentium officia non quod vel rerum nihil.\nVeniam qui incidunt dolorum.\nMolestiae ea officia qui explicabo nulla repellat.", new DateTime(2020, 10, 26, 18, 26, 22, 257, DateTimeKind.Local).AddTicks(3206), "In qui labore omnis voluptas voluptates aspernatur consectetur pariatur.", 2, 6, 3, null, null },
                    { 1, new DateTime(2020, 7, 1, 1, 22, 9, 903, DateTimeKind.Local).AddTicks(937), "Sed voluptas quia dolores expedita eius laborum ut qui aspernatur.\nMolestias sapiente pariatur fuga architecto sed.\nAutem repellendus maxime magni qui exercitationem rerum.\nDolorem magnam aut commodi nemo aut quaerat.\nEos sit veniam qui molestiae facere voluptatem.\nFacilis eum atque enim dolor facilis ea ipsum tempora.", new DateTime(2020, 12, 11, 0, 30, 51, 72, DateTimeKind.Local).AddTicks(4501), "Quasi consectetur nesciunt doloribus.", 1, 1, 2, null, null },
                    { 19, new DateTime(2020, 7, 1, 17, 23, 52, 23, DateTimeKind.Local).AddTicks(6810), "Perspiciatis et laborum et cum recusandae rerum repellat.\nRepudiandae pariatur minus corrupti doloribus omnis ad aut repudiandae.\nNostrum qui assumenda qui eveniet.\nNon in nisi in quasi excepturi commodi et nam.\nUt vitae ea odio aut ut rem nobis inventore aspernatur.\nRepellendus itaque et nostrum recusandae explicabo.", new DateTime(2021, 6, 24, 0, 11, 12, 508, DateTimeKind.Local).AddTicks(1451), "In odit omnis dolores quo similique placeat ex.", 14, 7, 2, null, null },
                    { 7, new DateTime(2020, 7, 1, 16, 48, 47, 732, DateTimeKind.Local).AddTicks(5296), "Dolores esse quibusdam aut ut quidem nulla voluptatem.\nQuidem vitae sequi aut qui cumque adipisci quo quam.\nAlias quis voluptatibus.\nQui est aut.\nNihil quia occaecati occaecati totam laudantium.\nNobis cum quae saepe molestiae voluptas id reiciendis a consequuntur.", new DateTime(2021, 6, 16, 17, 9, 33, 157, DateTimeKind.Local).AddTicks(8012), "Rerum eum qui ad repudiandae eum laborum ut saepe.", 13, 7, 1, null, null },
                    { 12, new DateTime(2020, 7, 1, 15, 38, 2, 311, DateTimeKind.Local).AddTicks(5268), "Optio modi exercitationem quia in omnis alias.\nEsse a voluptatem porro quo voluptatem fuga eos consequatur sit.\nRepellendus labore excepturi eaque impedit minus rerum ut qui eum.\nOdio enim qui corrupti.", new DateTime(2021, 6, 17, 16, 35, 49, 466, DateTimeKind.Local).AddTicks(1287), "Quidem dolores ut dolores atque sapiente laborum sint.", 15, 3, 1, null, null },
                    { 3, new DateTime(2020, 7, 1, 1, 52, 33, 64, DateTimeKind.Local).AddTicks(5959), "Unde dignissimos libero minima quas aliquam.\nConsequuntur aliquid non.\nEligendi quia quidem nihil sit veritatis.", new DateTime(2020, 7, 25, 10, 38, 48, 657, DateTimeKind.Local).AddTicks(938), "Error sit sunt.", 14, 3, 3, null, null },
                    { 16, new DateTime(2020, 7, 1, 3, 15, 53, 91, DateTimeKind.Local).AddTicks(9868), "Consequatur est sed id nemo fugit illo.\nMinus numquam enim veritatis in sed molestias et.\nQui molestiae rerum voluPraesentium autem consequatur magnam et doloribus exercitationem.\nAut animi fuga cupiditate debitis atque nisi consequatur consequatur.\nCupiditate necessitatibus quo eos sequi earum et quis accusamus.ptatem omnis et.\nImpedit aliquid ducimus et officia mollitia.\nLibero voluptatem et libero.\nSint et architecto quae tenetur est.", new DateTime(2020, 9, 30, 9, 30, 46, 607, DateTimeKind.Local).AddTicks(1674), "Inventore nulla sit et eius tempore dignissimos ut velit.", 15, 2, 1, null, null },
                    { 15, new DateTime(2020, 6, 30, 19, 18, 15, 603, DateTimeKind.Local).AddTicks(7796), "Laborum omnis dicta.\nQuas qui saepe perspiciatis aut asperiores dolor dolore.\nAliquam temporibus repudiandae magnam non cum aut quia eius vel.\nAut aliquid officia ad.", new DateTime(2021, 4, 17, 7, 55, 46, 332, DateTimeKind.Local).AddTicks(5709), "Qui facilis suscipit.", 13, 2, 0, null, null },
                    { 2, new DateTime(2020, 7, 1, 14, 49, 39, 729, DateTimeKind.Local).AddTicks(8570), "Praesentium autem consequatur magnam et doloribus exercitationem.\nAut animi fuga cupiditate debitis atque nisi consequatur consequatur.\nCupiditate necessitatibus quo eos sequi earum et quis accusamus.", new DateTime(2020, 7, 20, 1, 5, 30, 339, DateTimeKind.Local).AddTicks(708), "Praesentium ut consequatur cumque eveniet suscipit amet officia.", 9, 2, 0, null, null },
                    { 20, new DateTime(2020, 7, 1, 7, 58, 39, 381, DateTimeKind.Local).AddTicks(6512), "Vitae temporibus adipisci similique voluptatum vel facere fugit vitae.\nEt qui qui quidem est odit eos quam.\nNisi natus dolores fugiat consequatur quo est dolorum.\nAccusamus voluptatibus expedita est expedita sapiente vero dolorem aspernatur commodi.\nDolore dolorem asperiores eligendi totam repudiandae ipsum.\nPorro illum quos error voluptatibus maxime totam.", new DateTime(2020, 12, 26, 4, 20, 28, 760, DateTimeKind.Local).AddTicks(7853), "Rerum a eum minus repellendus.", 7, 9, 3, null, null },
                    { 9, new DateTime(2020, 6, 30, 18, 10, 2, 977, DateTimeKind.Local).AddTicks(8316), "Ipsam quo soluta aut numquam aliquam sint.\nAliquam voluptas error fuga est et quae dolores.", new DateTime(2020, 11, 24, 10, 24, 59, 79, DateTimeKind.Local).AddTicks(3910), "Dolorem fugiat impedit nesciunt enim non.", 3, 9, 0, null, null },
                    { 13, new DateTime(2020, 6, 30, 18, 35, 39, 951, DateTimeKind.Local).AddTicks(3554), "Quas et quae optio ullam amet amet qui voluptatum.\nUt eos neque quia occaecati voluptas voluptatem modi consequatur doloribus.\nVoluptatem occaecati et.\nFuga deserunt nam porro nam nobis deserunt laboriosam asperiores.\nAutem voluptatem cumque amet totam ducimus unde officiis.\nEarum aspernatur qui maxime at voluptatem placeat.", new DateTime(2021, 2, 22, 4, 41, 5, 762, DateTimeKind.Local).AddTicks(6966), "Minima vel eum atque perspiciatis laborum officiis cum.", 5, 4, 2, null, null },
                    { 4, new DateTime(2020, 7, 1, 4, 35, 10, 467, DateTimeKind.Local).AddTicks(552), "Nisi esse accusamus dolorem blanditiis porro est dolores.\nExplicabo consequatur rem dignissimos odit praesentium.\nMolestiae facilis et tenetur.\nVoluptas quis sed et ab nulla omnis cupiditate.\nId sed et.", new DateTime(2020, 10, 19, 8, 59, 38, 281, DateTimeKind.Local).AddTicks(3374), "Repellendus itaque expedita est ut.", 3, 4, 2, null, null },
                    { 18, new DateTime(2020, 6, 30, 18, 22, 49, 901, DateTimeKind.Local).AddTicks(1512), "Occaecati nulla dignissimos deserunt.\nConsequatur doloremque quaerat porro inventore incidunt cumque nulla inventore sed.\nPorro delectus reiciendis occaecati nisi temporibus ea.\nFugiat eligendi quo fugit et nihil.", new DateTime(2021, 4, 3, 11, 23, 15, 662, DateTimeKind.Local).AddTicks(1481), "Praesentium aut rerum velit ad non.", 17, 8, 1, null, null },
                    { 11, new DateTime(2020, 7, 1, 4, 50, 46, 565, DateTimeKind.Local).AddTicks(9074), "Quod autem atque similique molestiae dicta quia.\nNulla nulla consequatur at sint enim et similique.\nFugit occaecati enim aut doloremque aliquid vero molestiae iste.\nQuaerat delectus id", new DateTime(2020, 12, 23, 9, 7, 45, 735, DateTimeKind.Local).AddTicks(4976), "Quod autem atque similique", 17, 8, 1, null, null },
                    { 8, new DateTime(2020, 7, 1, 15, 54, 51, 517, DateTimeKind.Local).AddTicks(8046), "Nihil eos minima sed.\nEst et assumenda voluptatem voluptatem illum doloribus.\nVoluptatem enim voluptatem et ut.", new DateTime(2020, 12, 29, 18, 35, 36, 313, DateTimeKind.Local).AddTicks(8043), "Et ipsa voluptatem non eos et similique sunt.", 16, 8, 2, null, null },
                    { 17, new DateTime(2020, 7, 1, 6, 42, 41, 935, DateTimeKind.Local).AddTicks(754), "Tenetur libero maiores fugit eos voluptatem id maxime dolores ducimus.\nMaiores omnis quia.\nNobis quas optio iste qui autem odit tempora qui ratione.\nAsperiores ut doloremque odio eius.\nQui porro sed autem ut sed.", new DateTime(2021, 6, 29, 7, 11, 51, 204, DateTimeKind.Local).AddTicks(436), "Eaque labore officia placeat est error blanditiis quos.", 17, 5, 3, null, null },
                    { 5, new DateTime(2020, 6, 30, 22, 51, 23, 273, DateTimeKind.Local).AddTicks(3812), "Rerum totam sit.\nVelit saepe iusto et repellat et consequuntur sit.\nVoluptate officiis pariatur ut ea.\nNeque ut sed voluptatem occaecati.\nDolor velit quaerat molestiae assumenda veritatis voluptatem.", new DateTime(2021, 6, 20, 5, 7, 3, 723, DateTimeKind.Local).AddTicks(486), "Voluptate neque vel molestiae dolor nulla voluptas voluptas optio.", 16, 5, 1, null, null },
                    { 14, new DateTime(2020, 6, 30, 22, 4, 36, 617, DateTimeKind.Local).AddTicks(5319), "Ipsum odio qui in dolorum aperiam ut.\nBlanditiis ratione sapiente quos est quia pariatur.\nSimilique repudiandae hic enim non neque magnam fugiat est.\nExcepturi accusamus non soluta inventore enim doloribus culpa veniam.\nImpedit sunt magni cumque autem.\nQuis suscipit culpa quia voluptatem.", new DateTime(2020, 10, 1, 0, 44, 11, 408, DateTimeKind.Local).AddTicks(6524), "Placeat impedit ullam eveniet corporis quidem doloribus.", 4, 1, 0, null, null },
                    { 10, new DateTime(2020, 7, 1, 14, 45, 13, 834, DateTimeKind.Local).AddTicks(6421), "Quia quam eveniet quisquam rerum voluptatum laboriosam repudiandae.\nVoluptate fugit esse eveniet ducimus sunt veniam a.\nSed qui et consequatur similique eum velit ipsa voluptates ut.", new DateTime(2020, 10, 13, 9, 22, 2, 731, DateTimeKind.Local).AddTicks(9262), "Dolorem eos incidunt eum perspiciatis officia ratione est rerum.", 18, 10, 2, null, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(650)",
                oldMaxLength: 650,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(550)",
                oldMaxLength: 550,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(1000)",
                oldMaxLength: 1000,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "TaskStates",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tasks",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(1000)",
                oldMaxLength: 1000,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Tasks",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(4000)",
                oldMaxLength: 4000,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(1000)",
                oldMaxLength: 1000,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Projects",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(4000)",
                oldMaxLength: 4000,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Positions",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(200)",
                oldMaxLength: 200);
        }
    }
}
