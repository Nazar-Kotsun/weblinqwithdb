using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using WebAPILinq.DAL.Models;
using WebAPILinq.DataBase;

namespace WebAPILinq.DAL.UnitOfWork.Repositories
{
    public class PositionRepository : IBaseRepository<PositionModel>
    {
        private readonly ProjectsLinqContext _dbContext;
        private readonly DbSet<PositionModel> _dbSet;

        public PositionRepository(ProjectsLinqContext projectsLinqContext)
        {
            _dbContext = projectsLinqContext;
            _dbSet = _dbContext.Set<PositionModel>();
        }

        public ValueTask<EntityEntry<PositionModel>> Create(PositionModel entity) => _dbSet.AddAsync(entity);

        public Task<PositionModel> GetById(int id) => _dbSet.AsNoTracking()
            .FirstOrDefaultAsync(p => p.Id == id);

        public Task<List<PositionModel>> GetAll() => _dbSet.ToListAsync();

        public async Task Update(PositionModel entity)
        {
            var local = await GetById(entity.Id);
            if (local != null)
            {
                _dbContext.Entry(local).State = EntityState.Detached;
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
        }
        public async Task Delete(int id) => _dbSet.Remove(await GetById(id));
    }
}