using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using WebAPILinq.DAL.Models;
using WebAPILinq.DataBase;
using WebAPILinq.Helpers;

namespace WebAPILinq.DAL.UnitOfWork.Repositories
{
    public class UserRepository : IBaseRepository<UserModel>
    {
        private readonly ProjectsLinqContext _dbContext;
        private readonly DbSet<UserModel> _dbSet;

        public UserRepository(ProjectsLinqContext projectsLinqContext)
        {
            _dbContext = projectsLinqContext;
            _dbSet = _dbContext.Set<UserModel>();
        }

        public ValueTask<EntityEntry<UserModel>> Create(UserModel entity) => _dbSet.AddAsync(entity);

        public Task<UserModel> GetById(int id) => _dbSet.FirstOrDefaultAsync((user) => user.Id == id);
        public Task<List<UserModel>> GetAll() => _dbSet.AsNoTracking().ToListAsync();
        
        public async Task Update(UserModel entity)
        {
            var local = await GetById(entity.Id);
            if (local != null) 
            {
                _dbContext.Entry(local).State = EntityState.Detached;
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task Delete(int id) => _dbSet.Remove(await GetById(id));
    }
}