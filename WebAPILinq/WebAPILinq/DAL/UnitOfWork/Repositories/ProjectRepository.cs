using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using WebAPILinq.DAL.Models;
using WebAPILinq.DataBase;
using WebAPILinq.Helpers;

namespace WebAPILinq.DAL.UnitOfWork.Repositories
{
    public class ProjectRepository : IBaseRepository<ProjectModel>
    {
        private readonly ProjectsLinqContext _dbContext;
        private readonly DbSet<ProjectModel> _dbSet;
        public ProjectRepository(ProjectsLinqContext projectsLinqContext)
        {
            _dbContext = projectsLinqContext;
            _dbSet = _dbContext.Set<ProjectModel>();
        }

        public ValueTask<EntityEntry<ProjectModel>> Create(ProjectModel entity) => _dbSet.AddAsync(entity);

        public Task<ProjectModel> GetById(int id) => _dbSet.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);

        public Task<List<ProjectModel>> GetAll() => _dbSet.AsNoTracking().ToListAsync();

        public async Task Update(ProjectModel entity)
        {
            var local = await GetById(entity.Id);
            if (local != null)
            {
                _dbContext.Entry(local).State = EntityState.Detached;
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task Delete(int id) => _dbSet.Remove(await GetById(id));
    }
}