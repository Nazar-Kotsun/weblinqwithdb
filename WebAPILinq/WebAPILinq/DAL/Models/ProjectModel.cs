using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;


namespace WebAPILinq.DAL.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        [ForeignKey("FK_User")]
        public int AuthorId { get; set; }
        [ForeignKey("FK_Team")]
        public int TeamId { get; set; }
        public decimal Price { get; set; }
        public List<TaskModel> Tasks { get; set; }
        
        public TeamModel Team { get; set; }
        
        public UserModel Author { get; set; }
        
        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"Deadline: {Deadline}\n" +
                   $"AuthorId: {AuthorId}\n" +
                   $"TeamId: {TeamId}";
        }
    }
}