using System;
using System.Collections.Generic;


namespace WebAPILinq.DAL.Models
{
    public class TeamModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public List<UserModel> Users { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"CreatedAt: {CreatedAt}";
        }
    }
}