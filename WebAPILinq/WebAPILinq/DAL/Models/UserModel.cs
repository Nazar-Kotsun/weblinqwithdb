using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace WebAPILinq.DAL.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public int PositionId { get; set; }

        public TeamModel Team { get; set; }

        public List<TaskModel> Tasks{ get; set; }

        public PositionModel Position { get; set; }

        public List<ProjectModel> Projects { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"FirstName: {FirstName}\n" +
                   $"LastName: {LastName}\n" +
                   $"Email: {Email}\n" +
                   $"Birthday: {Birthday}\n" +
                   $"RegisteredAt: {RegisteredAt}\n" +
                   $"TeamId: {TeamId}";
        }
    }
}