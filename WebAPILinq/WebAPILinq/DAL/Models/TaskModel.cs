using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace WebAPILinq.DAL.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        [ForeignKey("FK_project")]
        public int ProjectId { get; set; }
        
        [ForeignKey("FK_User")]
        public int PerformerId { get; set; }
        [ForeignKey("FK_TaskState")]
        
        public int TaskStateId { get; set; }
        
        public ProjectModel Project { get; set; }

        public UserModel Performer { get; set; }

        public TaskStateModel TaskState { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"StateId: {TaskStateId}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"FinishedAt: {FinishedAt}\n" +
                   $"ProjectId: {ProjectId}\n" +
                   $"PerformerId: {PerformerId}";
        }
    }
}
