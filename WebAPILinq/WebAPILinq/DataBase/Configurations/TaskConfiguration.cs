using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using WebAPILinq.DAL.Models;
using WebAPILinq.Helpers;

namespace WebAPILinq.DataBase.Configurations
{
    public class TaskConfiguration : IEntityTypeConfiguration<TaskModel>
    {
        public void Configure(EntityTypeBuilder<TaskModel> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(1000);
            builder.Property(t => t.Description).HasMaxLength(4000);
            builder.Property(t => t.CreatedAt).IsRequired();
            builder.Property(t => t.FinishedAt).IsRequired();
            builder.Property(t => t.PerformerId).IsRequired();
            builder.Property(t => t.ProjectId).IsRequired();

            builder.HasOne(t => t.Performer)
                .WithMany(p => p.Tasks)
                .HasForeignKey(p => p.PerformerId).OnDelete(DeleteBehavior.NoAction);
            builder.HasData(TaskModelFactory.CreateTasks());
        }
    }
    
}