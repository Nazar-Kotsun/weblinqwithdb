using Microsoft.EntityFrameworkCore;
using WebAPILinq.DAL.Models;
using WebAPILinq.DataBase.Configurations;

namespace WebAPILinq.DataBase
{
    public class ProjectsLinqContext : DbContext
    {
        public ProjectsLinqContext(DbContextOptions<ProjectsLinqContext> options) : base(options) { }

        public DbSet<ProjectModel> Projects { get; set; }
        public DbSet<TaskModel> Tasks { get; set; }
        public DbSet<UserModel> Users { get; set; }
        public DbSet<TeamModel> Teams { get; set; }
        public DbSet<TaskStateModel> TaskStates { get; set; }
        public DbSet<PositionModel> Positions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProjectConfiguration());
            modelBuilder.ApplyConfiguration(new TaskConfiguration());
            modelBuilder.ApplyConfiguration(new TeamConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new TaskStateConfiguration());
            modelBuilder.ApplyConfiguration(new PositionModelConfiguration());
        }
    }
}