using System;

namespace WebAPILinq.DAL.Models
{
    public class ProjectTasksDTO
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public int CountOfTasks { get; set; }
    }
}