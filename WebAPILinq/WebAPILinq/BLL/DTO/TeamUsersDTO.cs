using System.Collections.Generic;

namespace WebAPILinq.DAL.Models
{
    public class TeamUsersDTO
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<UserModel> ListUsers { get; set; }
    }
}