using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPILinq.BLL.DTO;
using WebAPILinq.BLL.Services.Interfaces;
using WebAPILinq.DAL.Models;
using WebAPILinq.DAL.UnitOfWork;

namespace WebAPILinq.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<ProjectModel>> GetAllProjects()
        {
            return await _unitOfWork.ProjectRepository.GetAll();
        }

        public async Task<IEnumerable<ProjectTasksDTO>> GetProjectsWithCountOfTasksByUserId(int userId)
        {
            IEnumerable<ProjectModel> projects = await GetAllProjects();
            
            var result= projects
                .GroupJoin(await _unitOfWork.TaskRepository.GetAll(),
                    project => project.Id,
                    task => task.ProjectId,
                    (project, task) => new ProjectTasksDTO()
                    {
                        ProjectId = project.Id,
                        ProjectName = project.Name,
                        AuthorId = project.AuthorId,
                        TeamId = project.TeamId,
                        CountOfTasks = task.ToList().Count
                    }).Where(project => project.AuthorId == userId);
            
            return result;
        }
        
        public async Task<IEnumerable<ProjectDetailedDTO>> GetProjectsDetailed()
        {
            IEnumerable<ProjectModel> projects = await GetAllProjects();
            
            var result = projects.GroupJoin(await _unitOfWork.TaskRepository.GetAll(),
                    project => project.Id,
                    task => task.ProjectId,
                    (project, tasks) => new
                    {
                        Project = project,
                        CountOfTAsks = tasks.ToList().Count,
                        MaxTask =  tasks
                            .OrderByDescending(task => task.Description.Length)
                            .FirstOrDefault(),
                        MinTask = tasks
                            .OrderBy(task => task.Name.Length)
                            .FirstOrDefault(),
                    }
                ).GroupJoin(await _unitOfWork.UserRepository.GetAll(),
                    project => project.Project.TeamId,
                    user => user.TeamId,
                    (project, users) => new ProjectDetailedDTO()
                    {
                        Project = project.Project,
                        TheLongestTask = project.MaxTask,
                        TheShortedTask = project.MinTask,
                        CountOfUsers = users
                            .Where(user => user.TeamId == project.Project.TeamId 
                                                           && project.Project.Description.Length > 20 
                                                           && project.CountOfTAsks < 3).ToList().Count
                    }
                    );
            return result;
        }

        public async Task AddProject(ProjectModel projectModel)
        {
            if (projectModel != null)
            {
                if (await _unitOfWork.TeamRepository.GetById(projectModel.TeamId) == null)
                { 
                    throw new InvalidOperationException("This team doesn't exist");
                }
                
                if (await _unitOfWork.UserRepository.GetById(projectModel.AuthorId) == null)
                {
                    throw new InvalidOperationException("This user doesn't exist");
                }
                
                await _unitOfWork.ProjectRepository.Create(projectModel);
                await _unitOfWork.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task DeleteProject(int projectId)
        {
            if (await _unitOfWork.ProjectRepository.GetById(projectId) == null)
            {
                throw new InvalidOperationException("This project doesn't exist");
            }
            
            // List<TaskModel> tasks = await _unitOfWork.TaskRepository.GetAll();
            //
            // for (int i = 0; i < tasks.Count; i++)
            // {
            //     if (tasks[i].ProjectId == projectId)
            //     {
            //         await _unitOfWork.TaskRepository.Delete(tasks[i].Id);
            //     }
            // }
            
            await _unitOfWork.ProjectRepository.Delete(projectId);
            await _unitOfWork.SaveChanges();
        }

        public async Task UpdateProject(ProjectModel projectModel)
        {
            if (projectModel != null)
            {
                if (await _unitOfWork.UserRepository.GetById(projectModel.AuthorId) == null)
                {
                    throw new InvalidOperationException("This user doesn't exist");
                }

                if (await _unitOfWork.ProjectRepository.GetById(projectModel.Id) == null)
                {
                    throw new InvalidOperationException("This project doesn't exist");
                }
                
                if (await _unitOfWork.TeamRepository.GetById(projectModel.TeamId) == null)
                { 
                    throw new InvalidOperationException("This team doesn't exist");
                }
                
                await _unitOfWork.ProjectRepository.Update(projectModel);
                await _unitOfWork.SaveChanges();
            }

            else
            {
                throw new NullReferenceException();
            }
        }
    }
}