using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILinq.BLL.Services.Interfaces;
using WebAPILinq.DAL.Models;
using WebAPILinq.DAL.UnitOfWork;

namespace WebAPILinq.BLL.Services
{
    public class TeamService: ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TeamService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<TeamModel>> GetAllTeams()
        {
            return await _unitOfWork.TeamRepository.GetAll();
        }
        public async Task<IEnumerable<TeamUsersDTO>> GetTeamsWithUsers()
        {
            IEnumerable<TeamModel> teams = await GetAllTeams();
            IEnumerable<UserModel> users = await _unitOfWork.UserRepository.GetAll();
            
            var result = teams
                .GroupJoin(
                    users.OrderByDescending(u => u.RegisteredAt),
                    team => team.Id,
                    user => user.TeamId,
                    (team, user) => new TeamUsersDTO()
                    {
                        TeamId = team.Id,
                        TeamName = team.Name,
                        ListUsers = user
                    }
                ).Where(team => team.ListUsers
                    .All(user => DateTime.Now.Year - user.Birthday.Year > 10));
            
            return result;
        }

        public async Task AddTeam(TeamModel teamModel)
        {
            if (teamModel != null)
            {
                
                await _unitOfWork.TeamRepository.Create(teamModel);
                await _unitOfWork.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task DeleteTeam(int teamId)
        {
            if (await _unitOfWork.TeamRepository.GetById(teamId) == null)
            {
                throw new InvalidOperationException("This team doesn't exist");
            }
            
            // List<ProjectModel> projects = await _unitOfWork.ProjectRepository.GetAll();
            // List<UserModel> users = await _unitOfWork.UserRepository.GetAll();
            //
            // for (int i = 0; i < projects.Count; i++)
            // {
            //     if (projects[i].TeamId == teamId)
            //     {
            //         await _unitOfWork.ProjectRepository.Delete(projects[i].Id);
            //     }
            // }
            //
            // for (int i = 0; i < users.Count; i++)
            // {
            //     if (users[i].TeamId == teamId)
            //     {
            //         users[i].TeamId = null;
            //     }
            // }
            
            await _unitOfWork.TeamRepository.Delete(teamId);
            await _unitOfWork.SaveChanges();
        }

        public async Task UpdateTeam(TeamModel teamModel)
        {
            if (teamModel != null)
            {
                if ( await _unitOfWork.TeamRepository.GetById(teamModel.Id) == null)
                { 
                    throw new InvalidOperationException("This team doesn't exist");
                }
                
                await _unitOfWork.TeamRepository.Update(teamModel);
                await _unitOfWork.SaveChanges();
            }

            else
            {
                throw new NullReferenceException();
            }
        }
    }
}