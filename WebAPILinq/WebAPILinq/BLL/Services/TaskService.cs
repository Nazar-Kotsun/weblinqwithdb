using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILinq.BLL.Services.Interfaces;
using WebAPILinq.DAL.Models;
using WebAPILinq.DAL.UnitOfWork;

namespace WebAPILinq.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TaskService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<TaskModel>> GetAllTasks()
        {
            return  await _unitOfWork.TaskRepository.GetAll();
            
        }

        public async Task<IEnumerable<TaskModel>> GetTasksByUserId(int userId)
        {
            IEnumerable<TaskModel> tasks = await GetAllTasks();
            
            var result= tasks
                .Where(task => task.PerformerId == userId && task.Name.Length < 45);
            
            return result;
        }

        public async Task<IEnumerable<TaskDTO>> GetTasksFinishedInCurrentYear(int userId)
        {
            IEnumerable<TaskModel> tasks = await GetAllTasks();
            
            var result = tasks
                .Where(task => task.FinishedAt.Year == DateTime.Now.Year && task.PerformerId == userId)
                .Select(task => new TaskDTO{Id = task.Id, Name = task.Name});
            
            return result;
        }

        public async Task AddTask(TaskModel taskModel)
        {
            if (taskModel != null)
            {
                if (await _unitOfWork.UserRepository.GetById(taskModel.PerformerId) == null)
                {
                    throw new InvalidOperationException("This user doesn't exist");
                }

                if (await _unitOfWork.ProjectRepository.GetById(taskModel.ProjectId) == null)
                {
                    throw new InvalidOperationException("This project doesn't exist");
                }
                
                if (await _unitOfWork.TaskStateRepository.GetById(taskModel.TaskStateId) == null)
                {
                    throw new InvalidOperationException("This state doesn't exist");
                }

                await _unitOfWork.TaskRepository.Create(taskModel);
                await _unitOfWork.SaveChanges();
            }

            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task DeleteTask(int taskId)
        {
            if (await _unitOfWork.TaskRepository.GetById(taskId) == null)
            {
                throw new InvalidOperationException("This task doesn't exist");
            }
            
           await _unitOfWork.TaskRepository.Delete(taskId);
           await _unitOfWork.SaveChanges();
        }

        public async Task UpdateTask(TaskModel taskModel)
        {
            if (taskModel != null)
            {
                
                if (await _unitOfWork.TaskRepository.GetById(taskModel.Id) == null)
                { 
                    throw new InvalidOperationException("This task doesn't exist");
                }
                
                if (await _unitOfWork.UserRepository.GetById(taskModel.PerformerId) == null)
                {
                    throw new InvalidOperationException("This performer doesn't exist");
                }

                if (await _unitOfWork.ProjectRepository.GetById(taskModel.ProjectId) == null)
                {
                    throw new InvalidOperationException("This project doesn't exist");
                }
                
                await _unitOfWork.TaskRepository.Update(taskModel);
                await _unitOfWork.SaveChanges();
            }

            else
            {
                throw new NullReferenceException();
            }
        }
    }
}