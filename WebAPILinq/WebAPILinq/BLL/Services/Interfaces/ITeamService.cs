using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.BLL.Services.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamModel>> GetAllTeams();
        Task<IEnumerable<TeamUsersDTO>> GetTeamsWithUsers();
        Task AddTeam(TeamModel teamModel);
        Task DeleteTeam(int teamId);
        Task UpdateTeam(TeamModel teamModel);
    }
}