using System;
using System.Collections.Generic;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Helpers
{
    public static class ProjectModelFactory
    {
        public static IEnumerable<ProjectModel> CreateProjects()
        {
            return new List<ProjectModel>
            {
                new ProjectModel
                {
                    Id = 1,
                    Name = "Expedita amet quas id a.",
                    Description = "Ea ab omnis saepe rem vel et." +
                                  "\nIllo quaerat eos accusantium reiciendis dolores quibusdam ratione.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T03:15:53.2885115+00:00"),
                    Deadline = Convert.ToDateTime("2021-02-02T00:57:55.0677911+00:00"),
                    AuthorId = 8,
                    TeamId = 5,
                    Price = 20000
                },
                new ProjectModel
                {
                    Id = 2,
                    Name = "Totam autem hic atque suscipit.",
                    Description = "Aut quia id adipisci alias non mollitia.\n" +
                                  "Alias et at quia soluta quisquam aspernatur nemo molestias.\n" +
                                  "Vel id suscipit vero ipsa repudiandae nesciunt.\n" +
                                  "Provident veritatis maiores aut.\n" +
                                  "Iste et incidunt.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T23:19:09.5122629+00:00"),
                    Deadline = Convert.ToDateTime("2021-04-30T01:56:33.3939752+00:00"),
                    AuthorId = 6,
                    TeamId = 4,
                    Price = 1000
                }
                ,new ProjectModel
                {
                    Id = 3,
                    Name = "Eos illum eum minima quibusdam",
                    Description = "Quis dicta repudiandae consequatur et odio repudiandae occaecati.\n" +
                                  "Dolore fugit veniam dolorem aperiam consequatur cum sed officiis ut.\n" +
                                  "Exercitationem ea ducimus saepe id asperiores dignissimos molestiae repellat.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T12:59:04.683596+00:00"),
                    Deadline = Convert.ToDateTime("2020-11-07T12:49:38.5028811+00:00"),
                    AuthorId = 8,
                    TeamId = 4,
                    Price = 3120
                }
                ,new ProjectModel
                {
                    Id = 4,
                    Name = "Quia repellendus odit et eligendi.",
                    Description = "Voluptatem eaque accusamus maiores quo beatae quos doloremque.\nEos pariatur ea saepe atque.\nDelectus quidem voluptatem harum architecto repellat.\nCupiditate culpa consectetur illo occaecati et.\n" +
                                  "Cumque inventore voluptas tenetur.\n" +
                                  "Facilis quaerat sed praesentium.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T20:20:06.0433222+00:00"),
                    Deadline = Convert.ToDateTime("2020-09-05T12:42:26.2763701+00:00"),
                    AuthorId = 10,
                    TeamId = 3,
                    Price = 24000
                }
                ,new ProjectModel
                {
                    Id = 5,
                    Name = "Aspernatur vero quas et ipsum.",
                    Description = "Qui rem mollitia inventore nulla nam nam excepturi.\n" +
                                  "Quibusdam distinctio iste quo dolor.\n" +
                                  "Beatae consequatur qui est quo amet et quia.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T04:41:59.5052577+00:00"),
                    Deadline = Convert.ToDateTime("2021-05-04T02:01:44.6833676+00:00"),
                    AuthorId = 12,
                    TeamId = 2,
                    Price = 56000
                }
                ,new ProjectModel
                {
                    Id = 6,
                    Name = "Reprehenderit nam architecto sed aut.",
                    Description = "Molestiae incidunt praesentium dolor odit culpa voluptatibus maxime et nam.\n" +
                                  "Aut nam et laudantium omnis et sed.\n" +
                                  "Odio perspiciatis iure exercitationem possimus dicta minima.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T00:31:42.9236504+00:00"),
                    Deadline = Convert.ToDateTime("2020-07-30T00:13:23.134301+00:00"),
                    AuthorId = 14,
                    TeamId = 1,
                    Price = 4000
                }
                ,new ProjectModel
                {
                    Id = 7,
                    Name = "Quaerat omnis enim sint ex.",
                    Description = "Modi nisi quasi vero odio amet excepturi.\nOfficiis et a molestiae rerum.\n" +
                                  "Suscipit ea aut autem ipsa itaque nihil.\n" +
                                  "Eum et nihil eveniet accusantium ea quod temporibus.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T09:22:12.920059+00:00"),
                    Deadline = Convert.ToDateTime("2021-03-30T16:55:08.2800325+00:00"),
                    AuthorId = 14,
                    TeamId = 4,
                    Price = 6000
                }
                ,new ProjectModel
                {
                    Id = 8,
                    Name = "Ad vero qui culpa odit.",
                    Description = "Odio numquam quis quia ut sapiente facilis molestiae esse.\n" +
                                  "Perspiciatis enim totam repudiandae non sint similique.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T10:44:14.7854397+00:00"),
                    Deadline = Convert.ToDateTime("2020-08-26T03:56:19.7029479+00:00"),
                    AuthorId = 16,
                    TeamId = 2,
                    Price = 100000
                }
                ,new ProjectModel
                {
                    Id = 9,
                    Name = "In vitae neque soluta et.",
                    Description = "Deleniti voluptates tempora enim voluptas.\n" +
                                  "Voluptatem atque praesentium vel.\n" +
                                  "Itaque eum deleniti voluptas veniam.\n" +
                                  "Sapiente mollitia dolore placeat.\nAut molestiae error eaque cum.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T12:02:01.7186234+00:00"),
                    Deadline = Convert.ToDateTime("2021-05-12T06:50:23.127598+00:00"),
                    AuthorId = 7,
                    TeamId = 3,
                    Price = 1540
                }
                ,new ProjectModel
                {
                    Id = 10,
                    Name = "Atque ut nisi dignissimos quas.",
                    Description = "Rem ipsam et ipsa inventore.\n" +
                                  "Quibusdam id omnis fuga.\n" +
                                  "Et a porro ut deleniti.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T04:01:16.6284161+00:00"),
                    Deadline = Convert.ToDateTime("2021-01-17T17:40:52.3743928+00:00"),
                    AuthorId = 7,
                    TeamId = 5,
                    Price = 90000
                }
            };
        }
    }
}
