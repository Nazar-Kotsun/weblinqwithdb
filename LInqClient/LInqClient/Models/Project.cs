using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class Project
    {
        [JsonPropertyName("id")] 
        public int Id { get; set; }
        
        [JsonPropertyName("name")] 
        public string Name { get; set; }
        
        [JsonPropertyName("description")] 
        public string Description { get; set; }
        
        [JsonPropertyName("createdAt")] 
        public DateTime CreatedAt { get; set; }
        
        [JsonPropertyName("deadline")] 
        public DateTime Deadline { get; set; }
        
        [JsonPropertyName("authorId")] 
        public int AuthorId { get; set; }
        
        [JsonPropertyName("teamId")] 
        public int TeamId { get; set; }

        [JsonPropertyName("price")]
        public decimal Price { get; set; }

        public List<TaskModel> Tasks { get; set; }
        public User Author { get; set; }
        public Team? Team { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"Deadline: {Deadline}\n" +
                   $"AuthorId: {AuthorId}\n" +
                   $"TeamId: {TeamId}\n" +
                   $"Price: {Price}";
        }
    }
}