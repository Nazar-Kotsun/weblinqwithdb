using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;

namespace LInqClient.Models.DTO
{
    public class UserTasksDTO
    {
        
        [JsonPropertyName("userId")]
        public int UserId { get; set; }
        
        [JsonPropertyName("userFirstName")]
        public string UserFirstName { get; set; }
        
        [JsonPropertyName("userLastName")]
        public string UserLastName { get; set; }
       
        [JsonPropertyName("email")]
        public string Email { get; set; }
        
        [JsonPropertyName("birthday")]
        public DateTime Birthday { get; set; }
     
        [JsonPropertyName("userRegisteredAt")]
        public DateTime UserRegisteredAt { get; set; }

        [JsonPropertyName("teamId")]
        public int? TeamId { get; set; }
        
        [JsonPropertyName("positionId")]
        public int PositionId { get; set; }
        
        [JsonPropertyName("listTasks")]
        public IEnumerable<TaskModel> ListTasks { get; set; }

        public override string ToString()
        {
            return $"UserId: {UserId}\n" +
                   $"UserFirstName: {UserFirstName}\n" +
                   $"UserLastName: {UserLastName}\n" +
                   $"Email: {Email}\n" +
                   $"Birthday: {Birthday}\n" +
                   $"UserRegisteredAt: {UserRegisteredAt}\n" +
                   $"TeamId: {TeamId}\n" +
                   $"Count of tasks: {ListTasks.ToList().Count}";
        }
    }
}